import { Container, Grid, MenuItem, TextField, Typography, CardContent, Card} from "@mui/material";
import { useState } from "react";

function CarListFilter () {
    const brandsList = [
        {
            name:'BMW',
            value:'BMW'
        },
        {
            name:'VW',
            value:'VW'
        },
        {
            name:'Audi',
            value:'Audi'
        },
    ]

    const yearsList = [
        {
            name: '2018',
            value: '2018'
        },
        {
            name: '2019',
            value: '2019'
        },
        {
            name: '2020',
            value: '2020'
        },
    ]
    //==========QUẢN LÝ CÁC STATE===========//

    const [carsList] = useState(
        [
            {
              name: "BMW M6",
              url:
                "https://mediapool.bmwgroup.com/cache/P9/201411/P90169551/P90169551-the-new-bmw-m6-coup-exterior-12-2014-600px.jpg",
              release_year: 2020
            },
            {
              name: "VW Polo",
              url:
                "https://cdn.euroncap.com/media/30740/volkswagen-polo-359-235.jpg?mode=crop&width=359&height=235",
              release_year: 2018
            },
            {
              name: "Audi S6",
              url:
                "https://www.motortrend.com/uploads/sites/5/2020/03/6-2020-audi-s6.jpg?fit=around%7C875:492.1875",
              release_year: 2020
            },
            {
              name: "BMW M2",
              url:
                "https://imgd.aeplcdn.com/0x0/cw/ec/37092/BMW-M2-Exterior-141054.jpg?wm=0",
              release_year: 2019
            },
            {
              name: "Audi A3",
              url: "https://cdn.motor1.com/images/mgl/BEooZ/s3/2021-audi-s3.jpg",
              release_year: 2019
            }
          ]
    )
    const [filterBrands, setFilterBrands] = useState('')
    const [filterYears, setFilterYears] = useState('')

    //=========QUẢN LÝ CÁC HANDLE===========//

    const handleSelectBrands = (event) => {
        const selectBrands = (event.target.value);
        setFilterBrands(selectBrands);
    }

    const handleSelectYears = (event) => {
        const selectYears = (event.target.value);
        setFilterYears(selectYears);
    }

    const filterCars = carsList.filter((car) => {
       //Kiểm tra lọc theo thương hiệu và năm
       const brandMatch = filterBrands === '' || filterBrands === 'All' || car.name.includes(filterBrands);
       const yearMatch = filterYears === '' || filterYears === 'All' || car.release_year.toString() === filterYears;

       //Trar kết quả nếu cả hai điều kiện đúng
       return brandMatch && yearMatch;
    })
    console.log(filterCars)

    return(
        <Container sx={{
            mt: 3
        }} maxWidth='lg'>
            <Grid container>
                <Grid item md={12} sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                    <Typography sx={{
                        mr: 1
                    }}>Filter by Brands:</Typography>
                    <TextField
                        sx={{
                            width: '5rem'
                        }}
                        select
                        value={filterBrands}
                        label={'All'}
                        onChange={handleSelectBrands}
                    >
                        {brandsList.map((brand, index) => (
                            <MenuItem key={index} value={brand.value}>{brand.name}</MenuItem>
                        ))}
                    </TextField>
                </Grid>
            </Grid>
            <Grid container sx={{
                mt:2
            }}>
                <Grid item md={12} sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                    <Typography sx={{
                        mr: 3
                    }}>Filter by Years:</Typography>
                    <TextField
                        sx={{
                            width: '5rem'
                        }}
                        select
                        value={filterYears}
                        label={'All'}
                        onChange={handleSelectYears}
                    >
                        {yearsList.map((years, index) => (
                            <MenuItem key={index} value={years.value}>{years.name}</MenuItem>
                        ))}
                    </TextField>
                </Grid>
            </Grid>
            <Grid sx={{
                mt: 2
            }} container spacing={3}>
                {filterCars.length === 0 
                ? <Typography>Không có xe phù hợp</Typography> 
                :(filterCars.map((car, index) => (
                    <Grid key={index} item md={4}>
                    <Card sx={{ minWidth: 275 }}>
                        <CardContent>
                            <Typography>
                                {'Name: '}{car.name}
                            </Typography>
                            <Typography>
                                {'Year: '}{car.release_year}
                            </Typography>
                            <img src={car.url} alt={`car-${car.name}`} height={'250'} width={'100%'}/>
                        </CardContent>
                    </Card>
                </Grid> 
                ))) }
                             
            </Grid>
        </Container>
    )
}
export default CarListFilter;